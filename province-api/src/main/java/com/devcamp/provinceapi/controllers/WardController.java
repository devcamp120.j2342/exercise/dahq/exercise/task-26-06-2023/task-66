package com.devcamp.provinceapi.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CWard;
import com.devcamp.provinceapi.repositorys.DistrictRepository;
import com.devcamp.provinceapi.repositorys.WardRepository;

@CrossOrigin
@RestController
@RequestMapping("/")

public class WardController {
    @Autowired
    WardRepository wardRepository;
    @Autowired
    DistrictRepository districtRepository;

    @PostMapping("/ward/create/{district_id}")
    public ResponseEntity<CWard> createWard(@RequestBody CWard pWard, @PathVariable("district_id") int district_id) {

        try {
            Optional<CDistrict> cDisOptional = districtRepository.findById(district_id);
            CWard newWard = new CWard();
            newWard.setName(pWard.getName());
            newWard.setPrefix(pWard.getPrefix());
            newWard.setDistrict(pWard.getDistrict());
            newWard.setDistrict(cDisOptional.get());
            return new ResponseEntity<>(wardRepository.save(newWard), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward/details/{id}")
    public CWard getWardById(@PathVariable("id") int id) {
        return wardRepository.findById(id).get();
    }

    @DeleteMapping("/ward/delete/{id}")
    public ResponseEntity<CWard> deleteWard(@PathVariable("id") int id) {
        wardRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/ward/update/{id}")
    public ResponseEntity<CWard> updateWard(@PathVariable("id") int id, @RequestBody CWard pWard) {
        Optional<CWard> cWard = wardRepository.findById(id);
        if (cWard.isPresent()) {
            try {
                cWard.get().setName(pWard.getName());
                cWard.get().setPrefix(pWard.getPrefix());
                cWard.get().setDistrict(pWard.getDistrict());
                return new ResponseEntity<>(wardRepository.save(cWard.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }

    }

}
