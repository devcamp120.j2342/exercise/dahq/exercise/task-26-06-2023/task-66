package com.devcamp.provinceapi.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CDistrict;

public interface DistrictRepository extends JpaRepository<CDistrict, Integer> {

}
