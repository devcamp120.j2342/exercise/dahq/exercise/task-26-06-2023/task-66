package com.devcamp.provinceapi.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CProvince;

public interface ProvinceRepository extends JpaRepository<CProvince, Integer> {
    CProvince findByName(String name);

}
